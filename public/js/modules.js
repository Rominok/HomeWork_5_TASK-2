import growPopulation from "../HW/HW-2/growPopulation"
import rotatePlanet from "../HW/HW-2/rotatePlanet"
import populationMultiplyRate from "../HW/HW-2/populationMultiplyRate"
import Cataclysm from "../HW/HW-2/Cataclysm"
import randomPopulation from "../HW/HW-2/randomPopulation"

const constructorPlanet = {
  name: 'Mars',
  count: 0
}

const Planet = Object.assign(
  {},
  constructorPlanet,
  growPopulation,
  rotatePlanet,
  populationMultiplyRate,
  Cataclysm,
  randomPopulation
);
let population = Planet.randomPopulation;
let multiplyRate = Planet.populationMultiplyRate;

Planet.rotatePlanet(population,
  Planet.growPopulation,
  Planet.cataclysm,
  Planet.count,
  Planet.populationMultiplyRate);
