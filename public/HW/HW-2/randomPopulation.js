
let randomPopulation = {
  randomPopulation: getRandom(100000, 100000)
}

function getRandom(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export default randomPopulation;
